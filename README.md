# Homework Assignment - 5

Creating a test runner for a simple library

## Introduction

The project consist of a test runner and unit tests for a library with three simple functions:

- `stringReverse` - Takes a `string` as a parameter and returns a reversed version of it. If the parameter is invalid it returs `false`
- `stringToAlphabeticalOrder` - Takes a `string` as a parameter and returns an alphabetically ordered version of it. If the parameter is invalid it returs `false`
- `arrSum` - Takes an `array of numbers` and returns the sum of all of the items in that array. If the parameter is invalid it returs `false`

## Getting Started

### Prerequisites

- Node JS

### Installing

- Clone the repo

## Usage

- Run tests - `node test`
