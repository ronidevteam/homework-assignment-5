/**
 * All tests
 */

// Dependencies
const assert = require('assert');
const lib = require('./../app/lib');

// Create a container
const tests = {};

tests['stringReverse should return reversed string'] = () => {
    const reversedString = lib.stringReverse('abc');
    assert.equal(reversedString, 'cba');
};
tests['stringReverse should return false if invalid parameter'] = () => {
    const reversedString = lib.stringReverse(1);
    assert.equal(reversedString, false);
};

tests['stringToAlphabeticalOrder should return a given string in alphabetical order'] = () => {
    const orderedString = lib.stringToAlphabeticalOrder('test');
    assert.equal(orderedString, 'estt');
};
tests['stringToAlphabeticalOrder should return false if invalid parameter was passed'] = () => {
    const orderedString = lib.stringToAlphabeticalOrder(1234);
    assert.equal(orderedString, false);
};

tests['arrSum should return the sum of all numbers in a given array'] = () => {
    const sum = lib.arrSum([1, 2, 3, 4]);
    assert.equal(sum, 10);
};

tests['arrSum should return false if an invalid parameter was given'] = () => {
    const sum = lib.arrSum([1, 2, 3, '4']);
    assert.equal(sum, false);
};

tests['this test should fail'] = () => {
    const sum = lib.arrSum([1, 2, 3, 5]);
    assert.equal(sum, false);
};



// Export the module
module.exports = tests;