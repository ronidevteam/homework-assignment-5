/**
 * Test runner
 */


// Application logic for the test runner
const app = {};

app.tests = require('./tests');

// Test runner
app.runTests = () => {
    console.log();
    console.log('-----------------------------------------------------------');
    console.log('------------------ RUNNING UNIT TESTS ---------------------');
    console.log('');
    let counter = 0;
    let successes = 0;
    const limit = Object.keys(app.tests).length;
    let errors = [];
    for (let testName in app.tests) {
        if (app.tests.hasOwnProperty(testName)) {
            try {
                app.tests[testName]();
                counter++;
                successes++;
                console.log('\x1b[32m%s\x1b[0m', testName);
                if (counter == limit) {
                    app.testResults(limit, successes, errors);
                }
            } catch (e) {
                console.log('\x1b[31m%s\x1b[0m', testName);
                const error = {
                    'name': testName,
                    'error': e
                };
                counter++;
                errors.push(error);
                if (counter == limit) {
                    app.testResults(limit, successes, errors);
                }
            }
        }
    }
};

// Log the results (total number of tests, number of successes and failed) and all failed tests (test name and the reason it failed)
app.testResults = (totalAmount, successes, failedTests) => {
    console.log('');
    console.log('----------------------- RESULTS ---------------------------');
    console.log('');

    console.log('Total tests:  ' + totalAmount);
    console.log('\x1b[32m%s\x1b[0m', 'Tests passed: ' + successes);
    console.log('\x1b[31m%s\x1b[0m', 'Tests failed: ' + failedTests.length);
    console.log('');

    if (failedTests.length > 0) {
        console.log('--------------------- FAILED TESTS ------------------------');
        console.log('');
        failedTests.forEach(test => {
            console.log('\x1b[31m%s\x1b[0m', test.name);
            console.log(test.error);
            console.log();
        });
    }
    console.log('-------------------- END TEST SECTION ---------------------');
};


app.init = () => {
    // Run the tests
    app.runTests();
};

app.init();