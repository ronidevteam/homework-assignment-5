/**
 * Library
 */

// Reverse a string
const stringReverse = (str) => {
    if (str && typeof (str) == 'string') {
        return str.split('').reverse().join('');
    }
    return false;
};

// Sort a string in alphabetical order
const stringToAlphabeticalOrder = (str) => {
    if (str && typeof (str) == 'string') {
        return str.split('').sort('').join('').trim();
    }
    return false;
};

// Sums all numbers in array
const arrSum = arrOfNumbers => {
    if (arrOfNumbers && arrOfNumbers instanceof Array && arrOfNumbers.length > 0) {
        // Check if the array contains only numbers, else return false
        const invalidItems = arrOfNumbers.filter(item => {
            return typeof item != 'number';
        });
        if (invalidItems.length > 0) return false;

        // Sum up all items in the array
        return arrOfNumbers.reduce((a, b) => a + b, 0);
    }
    return false;
};


module.exports = {
    stringReverse,
    stringToAlphabeticalOrder,
    arrSum
};